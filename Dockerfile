# Stage 1
# Create image based on the official Node 10 image from dockerhub

FROM node:10 as node

# Set working directory
WORKDIR /app

# Copy package.json into working dir
COPY package.json /app/
COPY package-lock.json /app/

# Set working directory
WORKDIR /app

# Copy package.json into working dir
COPY package.json /app/
COPY package-lock.json /app/

# Install dependencies
RUN npm install

# Copy source code
COPY ./ /app/

# Run build
RUN npm run build -- --output-path=./dist/out --prod

# Stage 2
# Use Nginx to serve the app
FROM nginx:1.15

# Copy build files from node stage into nginx html dir
COPY --from=node /app/dist/out/ /usr/share/nginx/html

COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

# docker build -t ng-docker .
# docker run ng-docker -p 80:80
