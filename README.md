# Docker & Angular example

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

# Docker
1. Build image: `docker build -t ng-docker .`
2. Run image: `docker run -p 80:80 ng-docker`
